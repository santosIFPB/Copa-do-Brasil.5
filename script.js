let estadios = [];
class Estadio {
  constructor(nome, localizacao, capacidade) {
    this.nome = nome;
    this.localizacao = localizacao;
    this.capacidade = capacidade;
  }
}

function cadastrarEstadio() {
  let nome = byId("nomeDoEstadio").value;
  let localizacao = byId("localizacao").value;
  let capacidade = byId("capacidade").value;
  let estadio = new Estadio(nome, localizacao, capacidade);
  estadios.push(estadio);
  mostrarEstadio();
}

function mostrarEstadio() {
  let tabelaDosEstadios = byId("tabelaDosEstadios");
  let lista  = "<ol>"
  for(let estadio of estadios) {
    lista += `<li>Nome: ${estadio.nome} <br>
    Localização: ${estadio.localizacao} <br>
    Capacidade de pessoas: ${estadio.capacidade}; </li>`
  }
  lista += "</ol>" 
  tabelaDosEstadios.innerHTML = lista;
}

function byId(id) {
  return document.getElementById(id);
}



let times = [];
class Time {
  constructor(nome, localizacaoDoTime, tecnico, campeonatos) {
    this.nome = nome;
    this.localizacaoDoTime = localizacaoDoTime;
    this.tecnico = tecnico;
    this.campeonatos = campeonatos; 
  }
}

function cadastrarTime() {
  let nome = byId("nomeDoTime").value;
  let localizacaoDoTime = byId("localizacaoDoTime").value;
  let tecnico = byId("tecnico").value;
  let campeonatos = byId("campeonatos").value;
  let time = new Time(nome, localizacaoDoTime, tecnico, campeonatos);
  times.push(time);
  mostrarTimes();
}

function mostrarTimes() {
  let tabelaDosTimes = byId("tabelaDosTimes");
  let lista1  = "<ol>"
  for(let time of times) {
    lista1 += `<li>Nome: ${time.nome} <br>
    Localização: ${time.localizacaoDoTime} <br>
    Técnico responsável: ${time.tecnico} <br>
    Campeonatos jogados: ${time.campeonatos} </li>`
  }
  lista1 += "</ol>" 
  tabelaDosTimes.innerHTML = lista1;
}

function byId(id) {
  return document.getElementById(id);
}


let jogadores = [];
class Jogador {
  constructor(nome, numeroDaCamisa, nacionalidade, dataDeNascimento, posicao) {
    this.nome = nome;
    this.numeroDaCamisa = numeroDaCamisa;
    this.nacionalidade = nacionalidade;
    this.dataDeNascimento = dataDeNascimento;
    this.posicao = posicao;
  }
}

function cadastrarJogador() {
  let nome = byId("nomeDoJogador").value;
  let numeroDaCamisa = byId("numeroDaCamisa").value;
  let nacionalidade = byId("nacionalidade").value;
  let dataDeNascimento = byId("dataDeNascimento").value;
  let posicao = byId("posicao").value;
  let jogador = new Jogador (nome, numeroDaCamisa, nacionalidade, dataDeNascimento,posicao);
  jogadores.push(jogador);
  mostrarJogador();
}


function mostrarJogador() {
  let tabelaDosJogadores = byId("tabelaDosJogadores");
  let lista2  = "<ol>"
  for(let jogador of jogadores) {
    lista2 += `<li>Nome: ${jogador.nome} <br>
    Numero da Camisa: ${jogador.numeroDaCamisa} <br>
    Nacionalidade: ${jogador.nacionalidade} <br>
    Data de Nascimento: ${jogador.dataDeNascimento} <br>
    Posicao: ${jogador.posicao}; </li>`
  }
  lista2 += "</ol>" 
  tabelaDosJogadores.innerHTML = lista2;
}

function byId(id) {
  return document.getElementById(id);
}


let arbitros = [];
class Arbitro {
  constructor(nome, dataDeNasciment, nivelDeExperiencia) {
    this.nome = nome;
    this.dataDeNasciment = dataDeNasciment;
    this.nivelDeExperiencia = nivelDeExperiencia;
  }
}

function cadastrarArbitro() {
  let nome = byId("nome").value;
  let dataDeNasciment = byId("dataDeNasciment").value;
  let nivelDeExperiencia = byId("nivelDeExperiencia").value;
  let arbitro = new Arbitro(nome, dataDeNasciment, nivelDeExperiencia);
  arbitros.push(arbitro);
  mostrarArbitro();
}

function mostrarArbitro() {
  let tabelaDosArbitros = byId("tabelaDosArbitros");
  let lista3  = "<ol>"
  for(let arbitro of arbitros) {
    lista3 += `<li>Nome: ${arbitro.nome} <br>
    Data de Nascimento: ${arbitro.dataDeNasciment} <br>
    Nivel de Experiencia: ${arbitro.nivelDeExperiencia}; </li>`
  }
  lista3 += "</ol>" 
  tabelaDosArbitros.innerHTML = lista3;
}

function byId(id) {
  return document.getElementById(id);
}


let jogos = [];
class Jogo {
  constructor(mandante, visitante, hora, data) {
    this.mandante = mandante;
    this.visitante = visitante;
    this.hora = hora;
    this.data = data;
  }
}

function cadastrarJogo() {
  let mandante = byId("mandante").value;
  let visitante = byId("visitante").value;
  let hora = byId("hora").value;
  let data = byId("data").value;
  let jogo = new Jogo(mandante, visitante, hora, data);
  jogos.push(jogo);
  mostrarJogo();
}

function mostrarJogo() {
  let tabelaDosJogos = byId("tabelaDosJogos");
  let lista4  = "<ol>"
  for(let jogo of jogos) {
    lista4 += `<li>Time mandante: ${jogo.mandante} <br>
    Time visitante: ${jogo.visitante} <br>
    Horário: ${jogo.hora}; <br>
    Data: ${jogo.data};</li>`
  }
  lista4 += "</ol>" 
  tabelaDosJogos.innerHTML = lista4;
}

function byId(id) {
  return document.getElementById(id);
}